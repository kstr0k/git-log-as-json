# git-log-as-json

## _git log, but with JSON output_

`git-log-as-json` converts `git log` output to machine-parseable JSON. It features
- zero text-processing hacks &mdash; gathers information cleanly from `git log -z`
- minimal dependencies: no `nodejs`, just `perl` with the widely-available JSON module. Hey, I was almost going to write it in Bash.

## Copyright

`Alin Mr. <almr.oss@outlook.com>` / MIT license

## Usage

The following examples format the output with `jq`, but you can feed it to whatever else you need.

```
git log-as-json -n 3 | jq -SC . | less`
git log-as-json | jq -SC '.[].actions[] | .a,.files'
git-log-as-json ---fhist /tmp/fh.json
```

All arguments are passed to `git log` (except those starting with a triple dash: `---fhist`). JSON keys are exactly the `git log` [placeholders](https://www.git-scm.com/docs/git-log) (e.g. `H` is the `git` commit hash `%H`; `at` is the author UNIX timestamp `%at` etc). To specify `git` arguments (e.g. `-C dir`), run as git sub-command instead of calling directly (works as soon as the script is in the `$PATH`).

`---fhist` (experimental) summarizes all renames / adds / deletes in a JSON file, the format of which is described in the source; it may or may not be directly useful as it stands. As an example, `.from [""] [*].name` finds all files ever deleted (as revealed in the requested log). You could use it to code a generalized `git log --follow`, or to rescue deleted files etc.

### Sample output

```
[
  {
    "H": "bb97c90b77f97d263e8fad2442adfae6ee85345c",
    "actions": [
      {
        "a": "D",
        "files": [
          "doc/HOWTO.txt",
          ""
        ]
      }
    ],
    "ae": "almr.oss@outlook.com", "at": "1618658701",
    "ce": "almr.oss@outlook.com", "ct": "1618658701",
    "n": 1, "s": "Test delete"
  },
  {
    "H": "891c3f17e14b29e14aafef9d30b3d7d76e67fc4f",
    "actions": [
      {
        "a": "A",
        "files": [
          "",
          "doc/HOWTO.txt"
        ]
      }
    ],
    "ae": "almr.oss@outlook.com",
    "at": "1618658686",
    "ce": "almr.oss@outlook.com",
    "ct": "1618658686",
    "n": 2, "s": "Test add"
  },
  {
    "H": "f68d232c0b97d4ddce38a43fe5f8eabf129db21c",
    "actions": [
      {
        "a": "R",
        "files": [
          "README.md",
          "doc/README.md"
        ]
      }
    ],
    "ae": "almr.oss@outlook.com", "at": "1618658645",
    "ce": "almr.oss@outlook.com", "ct": "1618658645",
    "n": 3, "s": "Test move"
  },
  {
    "H": "aeada673fe97bf01da8edfb60b29839bf5b1d8af",
    "actions": [
      {
        "a": "A",
        "files": [
          "",
          "README.md"
        ]
      },
      {
        "a": "M",
        "files": [
          "git-log-as-json",
          "git-log-as-json"
        ]
      }
    ],
    "ae": "almr.oss@outlook.com", "at": "1618653495",
    "ce": "almr.oss@outlook.com", "ct": "1618657956",
    "n": 4, "s": "add README.md, modify script"
  },
  ...
]
```

Brief explanation:
- each commit consists of one or more actions
- `actions [i].a` is what you'd see in `git log --name-status` (`A` = add, `D` = delete, `M` = modify, `R` = rename)
- `actions [i].files` contains the source and destination file names (same for normal `M` commits).

# Installation

Just symlink or copy into `$PATH`. Needs `perl >= 5.28` with the `JSON` module (e.g. `apt install libjson-perl` on Debians). You may want [`jq`](https://stedolan.github.io/jq/) too.

## Other software

Found on the web, in no particular order:
- https://coderwall.com/p/pmamka/git-can-do-json-almost
- https://github.com/dreamyguy/gitlogg

### Self promotion

Check out my other `git` tools:
- [git worktree-path](https://gitlab.com/kstr0k/git-worktree-path/): convert to/from relative-path worktrees; run commands / scripts in all worktrees
- [git repo4each](https://gitlab.com/kstr0k/git-repo4each/): do stuff for multiple repos with multiple worktrees
- [git pretty-alias](https://gitlab.com/kstr0k/git-pretty-alias/): edit complex `git` shell-aliases in human-friendly format
